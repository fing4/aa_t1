import numpy as np
import pandas as pd
import yaml
from collections import Counter

with open("config.yml", "r") as f:
    config = yaml.safe_load(f)

funcionObjetivo = config["funcion_objetivo"]

# Función para calcular la entropía de un conjunto de datos
def calcular_entropia(data):
    # Contamos la frecuencia de cada clase en la columna objetivo
    clases, counts = np.unique(data[funcionObjetivo], return_counts=True)
    # Calculamos la proporción de cada clase
    proporciones = counts / counts.sum()
    # Calculamos la entropía
    entropia = -np.sum(proporciones * np.log2(proporciones))
    return entropia

# Función para calcular la ganancia de información para una característica dada
def ganancia_informacion(data, caracteristica):
    # Calculamos la entropía del conjunto de datos original
    entropia_original = calcular_entropia(data)
    # Obtenemos los valores únicos y sus frecuencias para la característica
    valores, counts = np.unique(data[caracteristica], return_counts=True)
    # Calculamos la entropía ponderada para cada valor de la característica
    entropia_subset_ponderada = 0
    for valor, count in zip(valores, counts):
        subset = data[data[caracteristica] == valor]
        # Calculamos la entropía para cada clase de la característica seleccionada
        entropia_subset_ponderada += (count / len(data)) * calcular_entropia(subset)
    # Calculamos la ganancia de información
    ganancia = entropia_original - entropia_subset_ponderada
    return ganancia


# Función recursiva para construir el árbol de decisión con hiperparámetros
def construir_arbol(data, caracteristicas_originales, min_samples_split, min_split_gain):
    # Si todas las instancias tienen la misma clase, retornamos la clase
    if len(np.unique(data[funcionObjetivo])) == 1:
        return data[funcionObjetivo].iloc[0]

    # Si el número de muestras es menor que min_samples_split, retornamos la clase más común
    # Si no quedan características (columnas) para dividir, retornamos la clase más común
    elif len(data) <= min_samples_split or len(caracteristicas_originales) == 0:
        # print("Salida por pocas instancias:", len(data)) if len(data) <= min_samples_split else None
        return data[funcionObjetivo].value_counts().idxmax()

    else:
        # Calculamos la ganancia de información para cada característica
        ganancias = {caracteristica: ganancia_informacion(data, caracteristica) for caracteristica in caracteristicas_originales}
        
        # Si ninguna ganancia supera min_split_gain, retornamos la clase más común
        if max(ganancias.values()) <= min_split_gain:
            # print("Salida por poca ganancia:", max(ganancias.values()))
            return data[funcionObjetivo].value_counts().idxmax()
        
        # Elegimos la característica con la mayor ganancia de información
        mejor_caracteristica = max(ganancias, key=ganancias.get)
        # print("Mejor caracteristica: ", mejor_caracteristica)
        # print("Max Ganancia: ", max(ganancias.values()))

        # Creamos el nodo del árbol para la mejor característica
        arbol = {mejor_caracteristica: {}}

        # Eliminamos la mejor característica de la lista de características originales
        caracteristicas_originales = [car for car in caracteristicas_originales if car != mejor_caracteristica]

        # Para cada valor único en la mejor característica, construimos un subárbol
        for valor in np.unique(data[mejor_caracteristica]):
            subset = data[data[mejor_caracteristica] == valor].drop(columns=[mejor_caracteristica])
            # Llamada recursiva para construir el subárbol
            subarbol = construir_arbol(subset, caracteristicas_originales, min_samples_split, min_split_gain)
            # Agregamos el subárbol al nodo actual
            arbol[mejor_caracteristica][valor] = subarbol

        return arbol
    

def predecir(arbol, df_test):
    def predecir_instancia(subarbol, instancia):
        if not isinstance(subarbol, dict):  # Si el nodo es una hoja
            return subarbol
        feature = list(subarbol.keys())[0]
        valor_de_instancia = instancia[feature]     # Obtenemos el valor de la característica
        
        # Encuentra la rama correspondiente al valor de la instancia
        subarbol_siguiente = subarbol[feature].get(valor_de_instancia)
        
        if subarbol_siguiente is None:  # Si no hay una rama que coincida, encuentra el valor más común
            subarbol_siguiente = max(subarbol[feature].values(), key=lambda x: list(subarbol[feature].values()).count(x))
        
        return predecir_instancia(subarbol_siguiente, instancia)  # Recursión en el subárbol

    predicciones = [predecir_instancia(arbol, instancia) for _, instancia in df_test.iterrows()]    # Recorremos las instancias de test
    return pd.Series(predicciones, index=df_test.index)

