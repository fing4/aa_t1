### Pasos para la ejecución del notebook

**1. Crear Entorno Virtual venv**
En una carpeta vacía, copiar el código, los datos y crear ambiente virtual:
python -m venv venv

**2. Iniciar venv y jupyter**
Ejecutar init.bat para activar el venv e iniciar jupyter

**3. Ejecución**
Abrir el notebook 'app.ipynb' y ejecutar las celdas en orden secuencial
